# Flight Booking Web Application

## Overview
This repository contains the source code for a flight booking web application. It includes JavaScript files for various functionalities, accompanied by HTML, Images and CSS for the front end. The application utilizes the Mapbox API for rendering maps to facilitate flight booking.

## JavaScript Files
- **createAccount.js**: Contains functions for creating user accounts and code that runs on the createAccount.html page.
- **doublePopup.js**: Manages the display of two separate popups on a screen, ensuring proper functionality when multiple popups are required.
- **login.js**: Handles account-related functions such as user login, logout, and code that runs on page load.
- **planRoutes.js**: Implements functions for displaying route summaries, removing routes, and finalizing trip planning, including code for displaying maps with specific route details.
- **popup.js**: Controls the display of a single popup.
- **previousTrips.js**: Manages previous trip data and related dialogs.
- **scheduledTrips.js**: Handles scheduled trip management, including removing trips and confirming removals, and replaces empty tables in scheduledTrips.html.
- **shared.js**: Defines major classes essential for the application, such as Route, Trip, Account, and AccountList. It also includes functions for handling local storage and initializing global variables.
- **tripPlanner.js**: Implements functions for trip planning, including displaying summaries, removing routes, and finalizing trips.
- **viewallRoutes.js**: Manages the display of all available routes, including web service requests, showing airport data, route data, removing layers, and planning pages.

## Usage
Users can interact with the frontend HTML pages to create accounts, log in/log out, book flights without logging in as guests or after logging in, plan routes, view previous and scheduled trips and explore available routes. The application utilizes the Mapbox API to provide a map interface for flight booking.

## Installation
1. Clone the repository to your local machine.
2. Open the HTML file `startup.html` in a web browser to start using the application.

## Dependencies
- Mapbox API: Used for rendering maps and facilitating route planning.

## Notes
Ensure that proper API keys and configurations are set up for Mapbox integration to ensure the proper functioning of the map features.
